package com.demo.springboot.service;

import com.demo.springboot.domain.dto.MovieListDto;

public interface CsvReaderService {
    MovieListDto readFromCsv();
}
