package com.demo.springboot.service;

import com.demo.springboot.domain.dto.MovieDto;
import com.demo.springboot.domain.dto.MovieListDto;
import org.springframework.stereotype.Service;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class CsvReaderImpl implements CsvReaderService {

    List<MovieDto> movies = new ArrayList<>();

        public MovieListDto readFromCsv(){
            try (BufferedReader br = new BufferedReader(new FileReader("movies.csv"))) {
                    String line;
                int index = 0;
                while ((line = br.readLine()) != null) {
                    if(index == 0){
                        index++;
                        continue;
                    }

                    String[] values = line.split(";");
                    MovieDto movieDto = new MovieDto(
                            Integer.parseInt(values[0]),
                            values[1],
                            Integer.parseInt(values[2].trim()),
                            values[3]
                    );
                    movies.add(movieDto);

                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            return new MovieListDto(movies);

        }

}
