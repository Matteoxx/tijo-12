package com.demo.springboot.rest;

import com.demo.springboot.domain.dto.MovieListDto;
import com.demo.springboot.service.CsvReaderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MovieApiController {

    private final CsvReaderService csvReaderService;
    private static final Logger LOGGER = LoggerFactory.getLogger(MovieApiController.class);

    @Autowired
    public MovieApiController(CsvReaderService csvReaderService) {
        this.csvReaderService = csvReaderService;
    }

    @RequestMapping(value = "/movies")
    @ResponseBody
    @CrossOrigin
    public MovieListDto getMovies() {

        LOGGER.info("--- get movies");
        return csvReaderService.readFromCsv();

    }
}
